package com.nx.arch.transactionmsg;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.druid.pool.DruidDataSource;
import com.nx.arch.transactionmsg.model.TxMsgDataSource;
import com.nx.arch.transactionmsg.model.Msg;
import com.nx.arch.transactionmsg.model.MsgInfo;
import com.nx.arch.transactionmsg.utils.Config;

public class MsgStorageTest {
    
    private static String url = String.format("jdbc:h2:mem:%s", MsgStorage.getTablename());
    
    private static String mysqlUrl = "jdbc:mysql://127.0.0.1:13306/arch_config?zeroDateTimeBehavior=convertToNull";
    
    private static String createTableSql = "CREATE TABLE mq_messages(" + "id bigint(2)  NOT NULL AUTO_INCREMENT," + "content VARCHAR(255) NOT NUll," + "topic char(255)  NOT NULL," + "tag char(255)," + "status tinyint," + "create_time  timestamp DEFAULT CURRENT_TIMESTAMP," + "PRIMARY KEY (id));";
    
    private MsgStorage storage;
    
    private DataSource dataSrc;
    
    public void setup()
        throws Exception, SQLException {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(org.h2.Driver.class.getName());
        dataSource.setUrl(url);
        dataSource.setUsername("sa");
        dataSource.setPassword("");
        // dataSource.setMaxWait(6000);
        List<String> topicList = new ArrayList<String>();
        topicList.add("test_topic");
        storage = new MsgStorage(new ArrayList<TxMsgDataSource>(), topicList);
        storage.init(new Config());
        HashMap<String, DataSource> data = new HashMap<String, DataSource>(10);
        data.put(url, dataSource);
        storage.setDataSourcesMap(data);
        dataSrc = dataSource;
        Connection con = dataSource.getConnection();
        PreparedStatement preparedStatement = con.prepareStatement(createTableSql);
        preparedStatement.execute();
    }
    
    @Before
    public void setupMysql() {
        List<TxMsgDataSource> list = new ArrayList<TxMsgDataSource>();
        TxMsgDataSource dbSrc = new TxMsgDataSource(mysqlUrl, "root", "123456");
        list.add(dbSrc);
        List<String> topics = new ArrayList<String>();
        topics.add("hello_topic");
        topics.add("hellotopic22332");
        // topics.add("hellotopictest22");
        storage = new MsgStorage(list, null);
        Config config = new Config();
        config.setMsgTableName("mq_messages_test");
        config.setHistoryMsgStoreTime(2);
        try {
            storage.init(config);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        dataSrc = storage.getDataSourcesMap().get(mysqlUrl);
    }
    
    private List<MsgInfo> selectAll(DataSource dataSrc)
        throws SQLException {
        Connection con = dataSrc.getConnection();
        PreparedStatement psmt = con.prepareStatement(String.format("select * from %s limit 100 ", MsgStorage.getTablename()));
        ResultSet rs = psmt.executeQuery();
        List<MsgInfo> list = new ArrayList<MsgInfo>(100);
        while (rs.next()) {
            MsgInfo msgInfo = new MsgInfo();
            msgInfo.setId(rs.getLong(1));
            msgInfo.setContent(rs.getString(2));
            msgInfo.setTopic(rs.getString(3));
            msgInfo.setTag(rs.getString(4));
            msgInfo.setStatus(rs.getInt(5));
            msgInfo.setCreateTime(rs.getTimestamp(6));
            list.add(msgInfo);
        }
        return list;
        
    }
    
    private void updateTimeStampToThreeDayAgo(MsgInfo msgInfo)
        throws SQLException {
        Connection con = this.dataSrc.getConnection();
        Date date = new Date(System.currentTimeMillis() - 4 * 24 * 60 * 60 * 1000);
        PreparedStatement psmt = con.prepareStatement("update mq_messages set create_time=? where id = ?");
        psmt.setTimestamp(1, new java.sql.Timestamp(date.getTime()));
        psmt.setLong(2, msgInfo.getId());
        int res = psmt.executeUpdate();
        con.close();
        System.out.println(res);
    }
    
    @Test
    public void testInsert()
        throws SQLException, InterruptedException {
        Connection con = dataSrc.getConnection();
        System.out.println(con.getAutoCommit());
        Map.Entry<Long, String> idUrlPair = MsgStorage.insertMsg(con, "helloworld", "hellotopic", "hellotag", 0);
        Long id = idUrlPair.getKey();
        Msg msg = new Msg(id, idUrlPair.getValue());
        MsgInfo info = storage.getMsgById(msg);
        System.out.println(info);
        assert (info != null);
        assert (info.getStatus() == MsgStorage.MSG_STATUS_WAITING);
        Map.Entry<Long, String> idUrlPair2 = MsgStorage.insertMsg(dataSrc.getConnection(), "helloworld2", "hellotopic2", "hellotag2", 0);
        Msg msg2 = new Msg(idUrlPair2.getKey(), idUrlPair2.getValue());
        System.out.println(storage.getMsgById(msg2));
        Long minId = storage.getMinIdOfWaitingMsg(dataSrc);
        System.out.println(minId);
        // Thread.sleep(100);
        TimeUnit.SECONDS.sleep(10);
        List<MsgInfo> list = storage.getWaitingMsg(dataSrc, 20);
        System.out.println("**********select wait**************");
        System.out.println(list);
        System.out.println("select wait");
        // assert (list.size() == 2);
        storage.updateSendMsg(msg2);
        MsgInfo msgInfo2Update = storage.getMsgById(msg2);
        assert (msgInfo2Update.getStatus() == MsgStorage.MSG_STATUS_SEND);
        // assert(storage.getWaitingMsg(dataSrc, 20).size() == 1);
        this.updateTimeStampToThreeDayAgo(msgInfo2Update);
        int deleteRes = storage.deleteSendedMsg(dataSrc, 20);
        System.out.println(String.format("deleteRes %s", deleteRes));
        List<MsgInfo> list2 = this.selectAll(dataSrc);
        System.out.println("delete msg end");
        System.out.println(list2);
        // assert(list2.size() == 1);
        
    }
    
    @Test
    public void testSelect()
        throws SQLException {
        List<MsgInfo> list = storage.getWaitingMsg(dataSrc, 50);
        System.out.println(list.size());
        for (MsgInfo info : list) {
            System.out.println(info.getId());
        }
    }
    
    @Test
    public void testSelectWaitIngMsg()
        throws SQLException {
        System.out.println("start ");
        // storage.insertMsg(dataSrc.getConnection(), "helloworldtest", "hellotopictest22", "hellotag2");
        List<MsgInfo> list = storage.getWaitingMsg(dataSrc, 50);
        for (MsgInfo info : list) {
            System.out.println(info);
        }
    }
    
    @Test
    public void testDeleteSendedMsg()
        throws SQLException {
        System.out.println(storage.deleteSendedMsg(dataSrc, 100));
    }
}
